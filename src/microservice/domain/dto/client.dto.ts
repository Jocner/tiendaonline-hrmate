import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class ClientDTO {

    @ApiProperty({
        description: 'Name',
        example: 'Luis',
    }) 
	@IsNotEmpty()
	@IsString()
	readonly name: string;

    @ApiProperty({
        description: 'Digite de DNI',
        example: '123',
    })
	@IsNotEmpty()
    @IsString()
	readonly dni: string;

    @ApiProperty({
        description: 'Digite su Email',
        example: 'luis@luis.com',
    })
	@IsNotEmpty()
	@IsEmail()
	readonly email: string;
    
    @ApiProperty({
        description: 'Digite su Nro Telefono',
        example: '9123456789',
    })
	@IsNotEmpty()
	@IsString()
	readonly phone: string;
    
    @ApiProperty({
        description: 'Digite su direccion',
        example: 'Santiago, Region Metropolitana, Chile',
    })
    @IsNotEmpty()
    @IsString()
    readonly address: string;
    
}



