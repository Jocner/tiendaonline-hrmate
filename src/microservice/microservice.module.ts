import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import * as dotenv from 'dotenv';
import { Client, ClientSchema } from './domain/schema/client.schema';
import { ClientOrder } from './infraestructure/persistence/client-order';
import { ClientController } from './infraestructure/controllers/client.controller';
import { Discount } from './application/case/discount';
import { FiltrarController } from './infraestructure/controllers/filtrar.controller';
dotenv.config();

@Module({
    imports: [
        MongooseModule.forRoot(process.env.DB),
        MongooseModule.forFeature([
            {
              name: Client.name,
              schema: ClientSchema,
            },
          ]),
      ],
    providers: [ ClientOrder, Discount ],  
    controllers: [ClientController, FiltrarController] 
})
export class MicroserviceModule {}
