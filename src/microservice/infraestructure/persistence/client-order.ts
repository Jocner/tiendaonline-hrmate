import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Client, ClientDocument } from "../../domain/schema/client.schema";
import { OrderDTO } from '../../domain/dto/order.dto';


@Injectable()
export class ClientOrder {
	constructor(@InjectModel(Client.name) private orderModel: Model<ClientDocument>) { }

	async newOrder(order: any): Promise<any> {
		try {
		
		const newOrder = await this.orderModel.create(order);
		return newOrder;

		} catch(er) {
			console.log(er);
		}
	}

	async findAll(): Promise<any> {
		try{

	    const data = await this.orderModel.find(); 		

		return data

		} catch(er) {
			console.log(er);
		}

	}

	async findById(id: string): Promise<any> {
		try{
		const result = await this.orderModel.findById({_id: id});
        
		return result;

		}catch(er) {
			console.log(er);
		}
	}

	async updateId(id: string, orderDTO: OrderDTO): Promise<any> {
		try{
		const result = await this.orderModel.findByIdAndUpdate({_id: id}, orderDTO, { new: true });
        
		return result;

		}catch(er) {
			console.log(er);
		}
	}

	async deleteId(id: string): Promise<any> {
		try{
			const result = await this.orderModel.findByIdAndDelete({_id: id});
			
			return result;
	
		}catch(er) {
			console.log(er);
		}
        
	}

	async credi(): Promise<any> {
		try{
			const credito = 'crédito disponible';
			const result = await this.orderModel.find({discount: credito});
	
			
			return result;
	
		}catch(er) {
			console.log(er);
		}
        
	}
}    