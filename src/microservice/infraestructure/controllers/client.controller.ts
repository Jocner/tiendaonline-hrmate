import { Controller, HttpStatus } from '@nestjs/common';
import { Body, Get, Param, Post, Put, Delete, Req, Res, Response, Query } from '@nestjs/common/decorators'; 
import { ClientOrder } from '../persistence/client-order';
import { OrderDTO } from '../../domain/dto/order.dto';
import { ClientDTO } from '../../domain/dto/client.dto';
import { Discount } from '../../application/case/discount';

@Controller('client')
export class ClientController {
    constructor(private readonly client: ClientOrder,
                private readonly discount: Discount
                ) {}

    @Post('/')
    async register(@Body()clientDTO: ClientDTO): Promise<any> {

        try {

        const apply = await this.discount.apply();

        const data = { name: clientDTO.name, dni: clientDTO.dni, email: clientDTO.email, phone: clientDTO.phone, address: clientDTO.address, discount: apply}
    
        const client = await this.client.newOrder(data);
           

            return client;
            
        } catch (err) {
            console.log(err);
        }

    }
    
    @Get('/')
    async all():Promise <any> {

        try {

            const result = await this.client.findAll();

            return result;
        } catch(err){
            console.log(err);
        }

    }

    @Get(':id')
    async allId(@Param('id') id:string):Promise <any> {

        try {

            const result = await this.client.findById(id);

            return result;
        } catch(err){
            console.log(err);
        }

    }

    @Put(':id')
    async putId(@Param('id') id:string, @Body() orderDTO: OrderDTO):Promise <any> {

        try {

            const result = await this.client.updateId(id, orderDTO);

            return result;
        } catch(err){
            console.log(err);
        }

    }

    @Delete(':id')
    async drop(@Res() res, @Query('id') id: string): Promise<any> {

        try {

            const result = await this.client.deleteId(id);

            return res.status(HttpStatus.OK).json({
                msg: 'client delete success!!!',
                result
            });
    

        } catch(err){
            console.log(err);
        }

    }


    
}
