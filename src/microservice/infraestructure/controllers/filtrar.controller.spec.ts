import { Test, TestingModule } from '@nestjs/testing';
import { FiltrarController } from './filtrar.controller';

describe('FiltrarController', () => {
  let controller: FiltrarController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FiltrarController],
    }).compile();

    controller = module.get<FiltrarController>(FiltrarController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
