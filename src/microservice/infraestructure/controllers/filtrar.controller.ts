import { Controller } from '@nestjs/common';
import { Get } from '@nestjs/common/decorators';
import { ClientOrder } from '../persistence/client-order';

@Controller('filtrar')
export class FiltrarController {
    constructor(private readonly client: ClientOrder,
        ) {}

        @Get('/')
        async credito():Promise <any> {
    
            try {
    
                const result = await this.client.credi();
       
                return result;
            } catch(err){
                console.log(err);
            }
    
        }    

}
