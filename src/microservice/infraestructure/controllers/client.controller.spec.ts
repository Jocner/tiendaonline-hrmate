import { Test, TestingModule } from '@nestjs/testing';
import { ClientController } from './client.controller';
import { ClientOrder } from '../persistence/client-order';


describe('ClientController', () => {
  let controller: ClientController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClientController],
      providers: [ClientOrder]
    }).compile();

    controller = module.get<ClientController>(ClientController);
  });

  it('should be defined', () => {
    expect(controller.all).toBeDefined();
  });
  it('should be defined', () => {
    expect(controller.allId).toBeDefined();
  });
  it('should be defined', () => {
    expect(controller.drop).toBeDefined();
  });
  it('should be defined', () => {
    expect(controller.putId).toBeDefined();
  });
  it('should be defined', () => {
    expect(controller.register).toBeDefined();
  });
});
