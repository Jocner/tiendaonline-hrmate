import { Injectable } from '@nestjs/common';



@Injectable()
export class Discount {

   async apply():Promise<any> { 

    let descuento = await Math.floor(Math.random() * 2);

    const result = descuento == 1 ? 'crédito disponible' : 'sin crédito disponible';
 
    return result;
   }
}
