import { Test, TestingModule } from '@nestjs/testing';
import { Discount } from './discount';


  describe('Discount', () => {  
  let service: Discount;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Discount],
    }).compile();

    service = module.get<Discount>(Discount);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
